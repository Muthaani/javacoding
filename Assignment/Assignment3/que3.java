import java.util.Scanner;

class Date{
	private int month,day, year;

	public Date() {
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	public void displayDate() {
		System.out.println(this.getMonth()+"/"+this.getDay()+"/"+this.getYear());
	}

	
}

class que3{
	public static void main(String[] args) {
		Date dt = new Date();
		Scanner sc = new Scanner(System.in);

		System.out.print("Day   :  ");
		dt.setDay(sc.nextInt());
		System.out.print("Month :  ");
		dt.setMonth(sc.nextInt());
		System.out.print("Year  :  ");
		dt.setYear(sc.nextInt());

		dt.displayDate();
		
	}

}
