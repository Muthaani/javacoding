import java.util.Scanner;

class Employee{
	private String Firstname;
	private String Lastname;
	private double Monthlysalary;
	
	public Employee() {}

	public String getFirstname() {
		return Firstname;
	}

	public void setFirstname(String firstname) {
		Firstname = firstname;
	}

	public String getLastname() {
		return Lastname;
	}

	public void setLastname(String lastname) {
		Lastname = lastname;
	}

	public double getMonthlysalary() {
		return Monthlysalary;
	}

	public void setMonthlysalary(double monthlysalary) {
		if(Monthlysalary>=0)
		Monthlysalary = monthlysalary;
	}
	
	public double Earlysalary() {
		return 12*this.Monthlysalary;
	}
	
	public void acceptrecord(){
		Scanner sc=new Scanner(System.in);
		System.out.printf("First name     :  ");
		this.setFirstname(sc.nextLine());
		System.out.printf("Last name      :  ");
		this.setLastname(sc.nextLine());
		System.out.printf("Monthly salary :  ");
		this.setMonthlysalary(sc.nextDouble());
		System.out.println();
	}

	public void printrecord(){
		System.out.println();
		System.out.println("First name      :  "+this.getFirstname());
		System.out.println("Last name       :  "+this.getLastname());
		System.out.println("Monthly salary  :  "+this.getMonthlysalary());
		
		System.out.printf("Yearly salary   :  %.2f\n",this.Earlysalary());
		this.setMonthlysalary(this.getMonthlysalary()*1.1);
		System.out.println("After 10% raise ");		
		System.out.printf("Yearly salary   : %.2f\n ",this.Earlysalary());

	} 
 
	
}

class que2 {

	public static void main(String[] args) {
		Employee e1=new Employee();
		Employee e2=new Employee();	
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Enter first employee details : ");
		e1.acceptrecord();
		System.out.println("Enter second employee details : ");
		e2.acceptrecord();		  
		e1.printrecord();
		System.out.println();
		e2.printrecord();


		
	}

}
