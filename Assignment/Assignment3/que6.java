import java.util.Scanner;
import java.io.*;

class B{
	private float monthCharges;
	private float callCharges;
	private float smsCharges;
	private int freeCalls;
	private int discCalls;
	
	public B(float monthCharges, float callCharges, float smsCharges, int freeCalls, int discCalls) {
		super();
		this.monthCharges = monthCharges;
		this.callCharges = callCharges;
		this.smsCharges = smsCharges;
		this.freeCalls = freeCalls;
		this.discCalls = discCalls;
	}
	
	public float billing(int call,int sms) {
		float sum;
		//call for which 100% charge applicable
		sum=(call-this.freeCalls-this.discCalls)*this.callCharges;		
		
		//call for which 50% charge applicable
		if((call-this.freeCalls)<50)
			sum=sum+(call-this.freeCalls)*this.callCharges*.5f;
		else
		sum=sum+(this.discCalls)*this.callCharges*.5f;
	
		//sms charge
		sum=sum+smsCharges*sms;
			
		//monthly  charge applicable
		sum=sum+ this.monthCharges;
		
		//12.5% gst 
		sum=sum*1.125f;	
		
		return sum;
	}
	
}

 class que6{

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int call,sms;
		
		System.out.println("Enter no of calls: ");
		call=sc.nextInt();
		System.out.println("Enter no of sms: ");
		sms=sc.nextInt();
		
		System.out.println("Enter plan :\n 1. Plan A\n 2. Plan B\n 3. Plan c");
		int choice=sc.nextInt();
		switch(choice) {
		case 1:
			B p1=new B(199f,1f,1f,50,50);
			System.out.printf("Total bill : %.2f",p1.billing(call, sms));
			break;
		case 2 :
			B p2=new B(299f,0.8f,0.75f,75,75);
			System.out.printf("Total bill : %.2f",p2.billing(call, sms));
			break;
		case 3:
			B p3=new B(399f,0.60f,0.50f,100,100);
			System.out.printf("Total bill : %.2f",p3.billing(call, sms));
			break;
		
		}
		
	}

}
