
public class IntNumber {

	public static void main(String[] args) {
		int num=20;      
        System.out.println("Binary equivalent      : "+Integer.toBinaryString(num));
        System.out.println("Octal equivalent       : "+Integer.toOctalString(num));
        System.out.println("Hexadecimal equivalent : "+Integer.toHexString(num));
    }

}