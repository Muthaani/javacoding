/*
3. Accept one integer number from command line and provide the
following functionalities
 Sum of digits. [e.g. for 1234 , 1 + 2 + 3 + 4 = 10]
 Reverse number [e.g. for 1234 ,4321]
 Check weather given number is palindrome or not.
[Check reverse and original no. are same or not]
 Perfect number. 28 = 1 + 2 + 4 + 7 + 14
[Perfect number is a positive integer that is the sum of
its proper positive divisors]
 Strong number. 145=> 1! + 4! + 5! = 1 + 24 + 120 = 145
[The sum of the factorials of digits of a number is equal to
the original number.]
 Armstrong number. 153 => 13 + 53 +33 = 1+125+27= 153
[The sum of the cubes of digits of a number is equal to the
original number.]
 Prime number. 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37,
41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97.
*/

class num{    
    public void sumdigit(int temp)
    {
        int sum=0;
        do
        {
            sum=sum+temp%10;
            temp=temp/10;            
        }while(temp!=0);
        System.out.println("Sum of digit : "+sum);

    }

    public void revnum(int temp){
        int num=0,i=0; 
        do{
            num=num*10 +temp%10;   
            temp=temp/10;   
        }while(temp!=0);
        System.out.println("rev of num : "+num);

    }

    public void ispal(int temp){
        int num=0,ori=temp;     
        do{
            num=num*10 +temp%10;   
            temp=temp/10;               
        }while(temp!=0);  
        if(ori==num)
        System.out.println("Number is palindrome");
        else
        System.out.println("Number is not palindrome");
    }
    
    public void isperfect(int temp)
    {
        int sum=0,i;
        for(i=1;i<=temp/2;i=i+1){
            if(temp%i==0)
            sum=sum+i;            
        }
        if(sum==temp)
           System.out.println("Number is perfect");
        else
           System.out.println("Number is not perfect");

    }
    
    public void isstrong(int temp){
        int ori=temp,res=0;
        while(temp!=0)
        {
            int i=temp%10,sum=1;
            while(i!=0)
            {
                sum=sum*i;
                i=i-1;
            }
            res=res+sum;
            temp=temp/10;
        }
        if(ori==res)
           System.out.println("Number is strong");
        else
           System.out.println("Number is not strong");
    }

    public void isarmstrong(int temp){
        int ori=temp,res=0,i;
        while(temp!=0)
        {   
            i=temp%10;        
            res=res+i*i*i;
            temp=temp/10;
        }
        if(ori==res)
           System.out.println("Number is armstrong");
        else
           System.out.println("Number is not armstrong");
    }

    public void listprime(){
        int i,j;
        boolean flag=false;
        System.out.printf("Prime numbers between 1-100 : ");
        for(i=2;i<=100;i=i+1){
            for(j=2;j<=i/2;j=j+1){
                if(i%j==0)
                    {
                        flag=true;
                         break;
                    }
            }
            if(flag==false)
               System.out.printf(" %d ",i);
            flag=false;
        }

    }
    



}



class que3 {
    public static void main(String []args){        
        int i;
         i=Integer.parseInt(args[0]);
         num n=new num();


         n.sumdigit(i);
         n.revnum(i);
         n.ispal(i);
         n.isperfect(i);
         n.isstrong(i);
         n.isarmstrong(i);
         n.listprime();
    }
}