package org;

public class LinkedList{	
	Node head=null;
	
	
	public LinkedList() {
		super();
	}


	class Node{		
		int data;
		Node next;
		public Node(int data){			
		this.data = data;
		this.next=null;
		}	   
	}
	
	
	public void addnode(int ele) {
		Node newNode=new Node(ele);
		if(this.head==null) {
			head=newNode;			
		}
		else {
			Node temp=head;
			while(temp.next!=null)
				 temp=temp.next;
			temp.next=newNode;
		}
	}
	
	public void show() {
		if(head!=null) {
		Node temp=this.head;
		while(temp!=null)
			{System.out.print(temp.data+"  ");
			temp=temp.next;
			}
		}
		else System.out.println("Empty !!!! ");
	}
	public void remove() {
		this.head=null;
	}
			
 }






