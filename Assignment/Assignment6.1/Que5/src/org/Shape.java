package org;

abstract class Shape implements Comparable<Shape>{
	public float area;
	public float perimeter;
	public abstract void calculatePerimeter();
	public abstract void calculateArea();
	public float getArea() {
		return this.area;
	}
	public float getPerimeter() {
		return this.perimeter;
	}
	/*
	public int compareTo(Shape f) {
		return (int)(this.area-f.area);
	}*/
	
	public int compareTo(Shape f) {
		return (int)(this.perimeter-f.perimeter);
	}
	
	
	
	
}

class Rect extends Shape{
	private float length;
	private float bredth;
		
	public float getLength() {
		return length;
	}
	public void setLength(float length) {
		this.length = length;
	}
	public float getBredth() {
		return bredth;
	}
	public void setBredth(float bredth) {
		this.bredth = bredth;
	}
	public Rect(float length, float bredth) {
		super();
		this.length = length;
		this.bredth = bredth;
		this.calculateArea();
		this.calculatePerimeter();
	}
	
	public void calculateArea() {
		this.area=this.length*this.bredth;
	}
	public void calculatePerimeter()
	{
		this.perimeter=2*(this.bredth+this.length);
	}
	
	public String toString() {
		return String.format("bredth : %-12.2f length : %-12.2f area : %-12.2f perimeter : %-6.2f", this.getBredth(),this.getLength(),this.getArea(),this.getPerimeter());
	}
	
}