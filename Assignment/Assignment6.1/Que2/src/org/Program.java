/*
 2. Write a class Date with suitable constructor, inspector and
mutator methods. Also override equals and toString methods.
Create array of object in main and sort that array
depending on year only.
*/

package org;

import java.util.Arrays;
import java.util.Scanner;

public class Program {
	static Scanner sc=new Scanner(System.in);
	
	private static final Date[] getInstance() {
		Date []d=new Date[5];
		for(int index=0;index<d.length;++index) {
			System.out.println("Enter new Date   ");
			System.out.print("Enter day   : ");
			int a=sc.nextInt();
			System.out.print("Enter month : ");
			int b=sc.nextInt();
			System.out.print("Enter year  : ");
			int c=sc.nextInt();
			d[index]=new Date(a,b,c);
		}
		return d;
	}
	
	public static void main(String[] args) {
		Date []d=Program.getInstance();
		Program.print(d);
		Arrays.sort(d);
		Program.print(d);		
	}

	private static void print(Date []d) {
		for(int index=0;index<d.length;++index) {
			System.out.println(d[index].toString());
		}
		System.out.println();
		
	}
}
