/*
 Write a class Address with suitable constructor, inspector
and mutator methods. Also override equals and toString
methods. Create array of object in main and sort that array
depending on state name only.

*/

package org;

import java.util.Arrays;
import java.util.Scanner;

public class Program {
	static Scanner sc=new Scanner(System.in);
	
	private static final Address[] getInstance() {
		Address []d=new Address[5];
		for(int index=0;index<d.length;++index) {
			System.out.println("Enter Address   ");
			System.out.print("Enter city name   : ");
			String a=sc.nextLine();
			System.out.print("Enter pincode     : ");
			String b=sc.nextLine();
			System.out.print("Enter state name  : ");
			String c=sc.nextLine();
			d[index]=new Address(a,b,c);
		}
		return d;
	}
	
	public static void main(String[] args) {
		Address []d=Program.getInstance();
		Program.print(d);
		Arrays.sort(d);
		Program.print(d);		
	}

	private static void print(Address []d) {
		for(int index=0;index<d.length;++index) {
			System.out.println(d[index].toString());
		}
		System.out.println();
		
	}
}
