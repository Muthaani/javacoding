package org;

public class Address implements Comparable<Address>{
	private String cityName;
	private String pincode;
	private String stateName;
	
	
	
	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public Address() {
		super();
	}

	public Address(String cityName, String pincode, String stateName) {
		super();
		this.cityName = cityName;
		this.pincode = pincode;
		this.stateName = stateName;
	}

	@Override
	public String toString() {
		return String.format("City name :%-20s Pincode : %-15s State name : %-20s ",this.getCityName(),this.pincode,this.getStateName());
	}
	
	@Override
	public int compareTo(Address obj) {
		return this.getStateName().compareTo(obj.getStateName());
	}
	
	
	
}