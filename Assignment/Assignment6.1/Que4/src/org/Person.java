package org;

import java.time.LocalDate;

public class Person implements Comparable<Person>{
	private String name;
	private LocalDate bdate;
	private String contactnumber;
		
	public Person() {
	}

	public Person(String name, LocalDate bdate, String contactnumber) {
		super();
		this.name = name;
		this.bdate = bdate;
		this.contactnumber = contactnumber;
	}

	@Override
	public String toString() {
		
		return String.format("Name :%-20s   Date : %-10s   Contact No.: %-20s ",this.getName(),this.bdate,this.getContactnumber());
	}
	
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getBdate() {
		return bdate;
	}

	public void setBdate(LocalDate bdate) {
		this.bdate = bdate;
	}

	public String getContactnumber() {
		return contactnumber;
	}

	public void setContactnumber(String contactnumber) {
		this.contactnumber = contactnumber;
	}

	@Override
	public int compareTo(Person obj) {
		return this.bdate.compareTo(obj.bdate);
	}
	
	
	
}