/*
 Write a class Person with suitable constructor, inspector
and mutator methods. Also override equals and toString
methods. Create array of object in main and sort that array
depending on birthdate only.

*/

package org;

import java.util.Date;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Scanner;

public class Program {
	static Scanner sc=new Scanner(System.in);
	
	private static final Person[] getInstance() {
		Person []d=new Person[5];
		for(int index=0;index<d.length;++index) {
			System.out.println("Enter details   ");
			System.out.print("Enter name            : ");
			String a=sc.nextLine();
			System.out.println("Enter Birthday        ");
			
			System.out.print("Enter day             : ");
			int e=sc.nextInt();
			System.out.print("Enter month           : ");
			int f=sc.nextInt();
			System.out.print("Enter year            : ");
			int g=sc.nextInt();			
			LocalDate z=LocalDate.of(g, f, e);	
			sc.nextLine();
			System.out.print("Enter contact number  : ");
			String c=sc.nextLine();
			d[index]=new Person(a,z,c);
		}
		return d;
	}
	
	public static void main(String[] args) {
		Person []d=Program.getInstance();
		Program.print(d);
		Arrays.sort(d);
		Program.print(d);		
	}

	private static void print(Person []d) {
		for(int index=0;index<d.length;++index) {
			System.out.println(d[index].toString());
		}
		System.out.println();
		
	}
}
