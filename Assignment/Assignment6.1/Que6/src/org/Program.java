/*
 A Statistician is collecting information of two kinds of
players
One is Cricket player and other is Football player.
There is always requirement of viewing income of players.
a. Income of players is calculated as follows.
1. Income of cricket player:
Rs. 10000 per match played
Rs. 20000 per man of the match award
2. Income of football player:
Rs. 8000 per match played
Rs. 3000 per goal made by the player
By assuming 10 different players accept the record for all
players and then calculate income using above formula. Also
sort array of players on the basis of income.
*/

package org;

import java.util.Arrays;
import java.util.Scanner;

public class Program {
	static Scanner sc=new Scanner(System.in);
	
	private static  final Player[] getInstance() {
		Player []p=new Player[5];			     
	
		for(int index=0;index<p.length;++index) {
			int m=0,r=0;
			System.out.print("1. Cricket\n2.Footballer\nEnter sport : ");
			if(sc.nextInt()==1) {				
				System.out.print("Enter no. of matched played            : ");
				m=sc.nextInt();
				System.out.print("Enter no. of man of the match received : ");
				r=sc.nextInt();	
				p[index]=new Cricketer(m,r);
			}
				
			else {				
				System.out.print("Enter no. of matched played            : ");
				m=sc.nextInt();
				System.out.print("Enter no.  of goals                    : ");
				r=sc.nextInt();	
				p[index]=new Footballer(m,r);
			}	
			
		}
		return p;
		
	}	
	

	public static void main(String[] args) {
		Player []p=Program.getInstance();
		Arrays.sort(p);
		Program.print(p);		
	}

	private static void print(Player []p) {
		for(int index=0;index<p.length;++index) {
		
		System.out.println(p[index].toString());
	}

}
}
