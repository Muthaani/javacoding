package org;

public class Player implements Comparable<Player>{
	private int match;		
	private double sal;
	
	public Player() {		
	}

	public Player(int match) {
		super();
		this.match = match;
		this.sal = 0.0;
	}

	public int getMatch() {
		return match;
	}

	public void setMatch(int match) {
		this.match = match;
	}

	public double getSal() {
		return sal;
	}

	public void setSal(double sal) {
		this.sal = sal;
	}
	@Override
	public int compareTo(Player p)
	{
        return (int)(this.getSal()-p.getSal());
	}
	
	@Override
	public String toString() {
		if(this instanceof Cricketer) {
			Cricketer c=(Cricketer)this;
			return String.format("Cricketer    matches :%5d     man of the match : %3d    Income : %10.2f", this.match,c.getManOfTheMatch(),this.sal);
		}
		else
		{
			Footballer c=(Footballer)this;
			return String.format("Footballer   matches :%5d     total goals      : %3d    Income : %10.2f", this.match,c.getGoals(),this.sal);
		}
			
		
	}

}


class Cricketer extends Player{
	private int manOfTheMatch;
	
	public Cricketer() {}

	public Cricketer(int match,int manOfTheMatch) {
		super(match);
		this.manOfTheMatch = manOfTheMatch;
		this.calculateSalary();
	}
	
	public int getManOfTheMatch() {
		return manOfTheMatch;
	}

	public void setManOfTheMatch(int manOfTheMatch) {
		this.manOfTheMatch = manOfTheMatch;
	}

	public void calculateSalary() {			
		this.setSal(this.getMatch()*10000 +this.manOfTheMatch*20000 );  		
	}	
	
}

class Footballer extends Player{
	private int goals;
	
	public Footballer() {}
	

	public int getGoals() {
		return goals;
	}


	public void setGoals(int goals) {
		this.goals = goals;
	}


	public Footballer(int match,int goals) {
		super(match);
		this.goals = goals;
		this.calculateSalary();
	}
	
	public void calculateSalary() {		
			this.setSal(this.getMatch()*8000 +this.goals*3000); 	
		
	}	
	
}








