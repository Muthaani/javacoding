/*
 Create class SavingsAccount.
Use a static variable annualInterestRate to store the annual
interest rate for all account holders. Each object of the class
contains a private instance variable savingsBalance indicating
the amount the saver currently has on deposit.
Provide method calculateMonthlyInterest to calculate the monthly
interest by multiplying the savingsBalance by annualInterestRate
divided by 12�this interest should be added to savings-Balance.
 Provide a static method modifyInterestRate that sets the
annualInterestRate to a new value. Write a program to test class
SavingsAccount. Instantiate two savingsAccount objects,
saver1 and saver2, with balances of $2000.00 and $3000.00,
respectively. Set annualInterestRate to 4%, then calculate the
monthly interest for each of 12 months and print the new balances for both savers. Next, set the annualInterestRate to
5%, calculate the next month�s interest and print
the new balances for both savers. 

*/
package org;

import java.util.Scanner;

public class Que7 {
	static Scanner sc=new Scanner(System.in);
	
	public static void callingMethod(SavingAccount saver) {
		TestSavingAccount test=new TestSavingAccount();	
		
	    System.out.println("\nMonthly Interest       Monthly updated balance");
	    test.calculations(saver);
	    test.print();	    		   
	   }

	public static void main(String[] args) {
	   SavingAccount saver1=new SavingAccount(2000.00);	   	
	   SavingAccount saver2=new SavingAccount(3000.00);
	     
	   
	   System.out.println("\nEnter interest rate");
	   SavingAccount.setInterestRate(sc.nextDouble());
	   
	   System.out.println("\nSaver 1 details :");
		Que7.callingMethod(saver1);
		
		System.out.println("\nSaver 2 details :");
		Que7.callingMethod(saver2);
		
		
		
		
		   System.out.println("\nEnter interest rate");
		   SavingAccount.setInterestRate(sc.nextDouble());
		   
		   System.out.println("\nSaver 1 details :\n");
			Que7.callingMethod(saver1);
			
			System.out.println("\nSaver 2 details :\n");
			Que7.callingMethod(saver2);
    
	   
     } 
	
}
