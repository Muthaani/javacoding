package org;

import java.util.Scanner;

public class Rational{
	private int num, denom;
	private static String[] operations = {"Addition","Subtraction","Multiplication","Division"}; 

	public Rational() {
		this.num = 0;
		this.denom = 1;
	}
	public void reducedForm(int[] arr) {
		int d; 
		d = findGcd(arr[0], arr[1]); 
		    arr[0] = arr[0] / d;
		    arr[1] = arr[1] / d;
	}

	public Rational(int num, int denom) {
		int[] arr = new int[] {num,denom};
		
		reducedForm(arr);
		this.num = arr[0];
		if(this.num == 0)
			this.denom = 0;
		else
			this.denom = arr[1];
	}
	
	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getDenom() {
		return denom;
	}

	public void setDenom(int denom) {
		this.denom = denom;
	}

	static int findGcd(int num, int denom) {          
	    if (denom == 0)  
	        return num;  
	    return findGcd(denom, num % denom);     
	} 
	
	public void acceptRecord() {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Numerator : ");
		setNum(sc.nextInt());
		System.out.print("Denominator : ");
		setDenom(sc.nextInt());
	}
	
	public void print() {
		System.out.println(this.num+" "+this.denom);
	}
	
	public static void printRecord(int[][] arr) {
		if( arr != null ) {
			System.out.println();
			for( int row = 0; row < 4; ++ row ) {
				System.out.printf("%-17s : %d/%d \n",operations[row],arr[row][0],arr[row][1]);
			}
		}
	}
	
	public static Rational addNumbers(Rational r1, Rational r2) {
		Rational result = new Rational((r1.num * r2.denom) + (r2.num * r1.denom) , r1.denom * r2.denom);
		return result;
	}
	
	public static Rational subtractNumbers(Rational r1, Rational r2) {
		Rational result = new Rational((r1.num * r2.denom) - (r2.num * r1.denom) , r1.denom * r2.denom);
		return result;
	}
	
	public static Rational multiplyNumbers(Rational r1, Rational r2) {
		Rational result = new Rational(r1.num * r2.num , r1.denom * r2.denom);
		return result;
	}
	
	public static Rational divideNumbers(Rational r1, Rational r2) {
		Rational result = new Rational(r1.num * r2.denom , r1.denom * r2.num);
		return result;
	}
	
	public String representAsString() {
		int[] arr = new int[] { this.getNum(),this.getDenom()};
		reducedForm(arr);
		return String.format("%d/%d", arr[0], arr[1] );
	}
	
	public String representInFloatingAsString() {
		
		return String.format("%.2f", (float)this.num/this.denom);
	}
}

