/*
 * Q.4 Write a class to simulate a Stack. 
*/
package org;

import java.util.Scanner;


public class Que4 {
	static Scanner sc=new Scanner(System.in);
	
	public static int list_menu() {
		System.out.println("Enter choice : ");
		System.out.println("0. Exit\n1. push\n2. pop");
		int choice=sc.nextInt();
		return choice;		
	}

	public static void main(String[] args) {
		int choice;
		Stack st=new Stack();
		System.out.println("Enter stack size : ");
		st.setSizeArray(sc.nextInt());
		
		while ((choice=Que4.list_menu())!=0) {
			switch(choice) {
			case 1:if(!st.ifFull())
			{
				System.out.println("Enter element : ");
			   st.push(sc.nextInt());
			}
			else 
				System.out.println("Stack is full !!!");
				break;
				
				
			case 2:if(!st.ifEmpty()) {
				System.out.println("Poped element : "+st.peek());
				  st.pop();
			}
			else
				System.out.println("Stack is empty : ");
				break;	
			}
			
		}
		
	

	}

}
