package org;

public class Stack {
	private int []arr;
	
	public static int top=-1;
	
	public void setSizeArray(int size) {
		arr=new int[size];
	}

	boolean ifFull() {
		if(top>=arr.length-1)
			return true;
		else 
			return false;
	}
	
	boolean ifEmpty() {
		if(top==-1)
			return true;
		else 
			return false;
	}
	
	public void push(int ele) {
		this.top=this.top+1;
		this.arr[this.top]=ele;
		
	}
	public int peek() {
		return this.arr[this.top];
	}
	
	public void pop() {
		this.top=this.top-1;
	}

}
