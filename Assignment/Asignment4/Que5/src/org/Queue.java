package org;

public class Queue {
private int []arr;
	
	public static int front=-1;
	public static int rear=-1;
	
	public void setSizeArray(int size) {
		arr=new int[size];
	}

	boolean ifFull() {
		if(rear>=arr.length-1)
			return true;
		else 
			return false;
	}
	
	boolean ifEmpty() {
		if(rear==-1||front==rear+1)
			return true;
		else 
			return false;
	}
	
	public void push(int ele) {
		rear=rear+1;
		this.arr[rear]=ele;
		if(front==-1)
			front=0;
		
	}
	public int peek() {
		return this.arr[front];
	}
	
	public void pop() {
		front=front+1;
	}

}
