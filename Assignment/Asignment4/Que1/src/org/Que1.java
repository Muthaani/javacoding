package org;

import java.util.Arrays;
import java.util.Scanner;


public class Que1{
	public static void main(String []args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter size of rows : ");
		int size=sc.nextInt();
		int [][]arr=new int[size][];
		createArr(arr);
		
		for (int i=0;i<arr.length;++i)
		{
			for (int j=0;j<arr[i].length;++j)
			{
				System.out.print(arr[i][j]+ "  ");
			}
			System.out.println();
		}
		
		
	}

	private static void createArr(int[][] arr) {	
		int i,j,size=arr.length,k;
		for(i=0;i<arr.length;++i) {
			arr[i]=new int[arr.length-i];
		}
		
		for( i=0;i<arr.length;++i)
		{
			j=size-i;		
			for(k=0;k<arr[i].length;++k) {
				arr[i][k]=j;
				--j;
			}
				
			}	
			System.out.println();
		}
		
	}

