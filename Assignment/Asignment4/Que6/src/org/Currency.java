package org;

import java.util.Scanner;

public class Currency {
	private static double []rates= { 0.013582, 0.011338, 0.010186, 1.416837, 0.012288 };
    
	private static String []currency={ "US Dollar", "Euro", "British Pound", "Japanese Yen", "Swiss Franc" };
	
	private static double []conversion=new double[5];
	
	public static double inr,max;
	
	
	static Scanner sc=new Scanner(System.in);
	
	public static void accept() {
		System.out.println("Enter amount to be converted in INR : ");
		inr=sc.nextDouble();
	}
	
	public static void convert() {
		for(int i=0;i<rates.length;++i)
		{
			conversion[i]=rates[i]*inr;
		}
	}
	
	public static void show() {
		for(int i=0;i<conversion.length;++i)
		{
			System.out.printf("INR %.3f to ",inr);
			System.out.printf(" %-20s :  %.3f\n",currency[i],+conversion[i]); 
		}
	}
}
