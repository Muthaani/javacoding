
 

package org;


enum TrafficLight{
	RED(30),GREEN(30),YELLOW(30);
	private int	duration;
	
	private TrafficLight(int duration) {
		this.duration=duration;
	}

	public int getDuration() {
		return duration;
	}	
	
}

public class Que9 {

	public static void main(String[] args) {
		
		TrafficLight[] t=TrafficLight.values();
		for(TrafficLight t1 : t)
			System.out.printf("%-15s   %-5d\n",t1.name(),t1.getDuration());
     } 	
}

