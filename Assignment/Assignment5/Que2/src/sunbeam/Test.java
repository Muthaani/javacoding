package sunbeam;

import java.util.Scanner;

import org.Area;
import org.Circle;
import org.Rectangle;
import org.Triangle;

public class Test {
	private static Scanner sc=new Scanner(System.in);
	
	public static int menulist() {
		System.out.println("\n0. Exit\n1. Functions of rectangle\n2. Functions of Circle\n3. Functions of Triangle");
		System.out.print("Enter choice : ");
		return sc.nextInt();		
	}
	
	
	public static void acceptRecord(Area a1) {
		System.out.println();
		
		if(a1 instanceof Rectangle) {
			Rectangle r=(Rectangle)a1;		
			System.out.print("Enter length     : ");
			r.setLength(sc.nextFloat());
			System.out.print("Enter bredth     : ");
			r.setBredth(sc.nextFloat());
		}
		else if(a1 instanceof Circle) {
			Circle c1=(Circle)a1;
			System.out.print("Enter radius     : ");
			c1.setRadius(sc.nextFloat());
			}
		
		else if(a1 instanceof Triangle) {
			
	         Triangle t1=(Triangle)a1;	
			System.out.print("Enter side 1     : ");
			t1.setSide1(sc.nextFloat());
			System.out.print("Enter side 2     : ");
			t1.setSide2(sc.nextFloat());
			System.out.print("Enter side 3     : ");
			t1.setSide3(sc.nextFloat());
			}
		a1.calculateArea();
		a1.calculatePerimeter();
	}
	
	public static void printRecord(Area a) {
	  System.out.println("Perimeter        : "+a.getPerimeter());
	  System.out.println("Area             : "+a.getArea());		
	}

}
