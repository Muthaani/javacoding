package sunbeam;

import org.*;

import java.util.Scanner;

public class Program {
	static Scanner sc=new Scanner(System.in);
	
	private static Area getinstance(int choice) {		
		switch(choice) {		
		
		case 1: return new Rectangle();	
		      
		case 2: return new Circle();	
	
			
		case 3:  return new Triangle();		
			
		}	
		return null;
	}

		
	
	public static void main(String[] args) {
		int choice;
		while((choice=Test.menulist())!=0) {
			Area a=getinstance(choice);			
			if(a!=null)
			{
				Test.acceptRecord(a);
				Test.printRecord(a);
			}
		}
	}
}

	