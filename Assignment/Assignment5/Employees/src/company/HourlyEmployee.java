package company;

public class HourlyEmployee extends Employee{
	private float hours;
	private float hourWages;
	
	public HourlyEmployee() {
		this.hours=0;
		this.hourWages=0.0f;
	}
	public HourlyEmployee(String fName,String lName,int SSN,float hours,float hourWages) {
		super(fName,lName,SSN);
		this.hours=hours;
		this.hourWages=hourWages;
	}
	public float getHours() {
		return hours;
	}
	public void setHours(float hours) {
		this.hours = hours;
	}
	public float getHourWages() {
		return hourWages;
	}
	public void setHourWages(float hourWages) {
		this.hourWages = hourWages;
	}
	
	
}
