package company;

public class CommissionEmployee extends Employee{
	private float grossSales;
	private float commRate;
	
	public CommissionEmployee() {
		this.grossSales=0.0f;
		this.commRate=0.0f;
	}
	
	public CommissionEmployee(String fName,String lName,int SSN,float grossSales,float commRate) {
		super(fName,lName,SSN);
		this.grossSales=grossSales;
		this.commRate=commRate;
	}
	
	public float getGrossSales() {
		return grossSales;
	}
	public void setGrossSales(float grossSales) {
		this.grossSales = grossSales;
	}
	public float getCommRate() {
		return commRate;
	}
	public void setCommRate(float commRate) {
		this.commRate = commRate;
	}
	
	
	

}
