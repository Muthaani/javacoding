package company;

public class BasePlusCommissionEmployee extends Employee{	
	private float baseSalary;
	private float commRate;
	private float grossSales;
	
	public  BasePlusCommissionEmployee() {		
		
	}	

	
	public float getBaseSalary() {
		return baseSalary;
	}


	public void setBaseSalary(float baseSalary) {
		this.baseSalary = baseSalary;
	}


	public float getCommRate() {
		return commRate;
	}


	public void setCommRate(float commRate) {
		this.commRate = commRate;
	}


	public float getGrossSales() {
		return grossSales;
	}


	public void setGrossSales(float grossSales) {
		this.grossSales = grossSales;
	}


	
	
	
	

}
