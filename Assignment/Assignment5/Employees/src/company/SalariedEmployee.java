package company;

public class SalariedEmployee extends Employee {
	private float salary;
	public SalariedEmployee() {
		this.salary=0.0f;
	}
	public SalariedEmployee(String fName,String lName,int SSN,float salary) {
		super(fName,lName,SSN);
		this.salary=0.0f;
	}
	public float getSalary() {
		return salary;
	}
	public void setSalary(float salary) {
		this.salary = salary;
	}
	
	
		

}
