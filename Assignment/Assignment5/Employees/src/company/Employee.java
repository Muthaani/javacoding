package company;

public class Employee {
	private String fName;
	private String lName;
	private int SSN;
	
	public Employee() {
		this.fName="First Name";
		this.lName="Last Name";
		this.SSN=0;
	}
	
	public Employee(String fName,String lName,int SSN) {
		this.fName=fName;
		this.lName=lName;
		this.SSN=SSN;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public int getSSN() {
		return SSN;
	}

	public void setSSN(int sSN) {
		SSN = sSN;
	}
	
   
}
