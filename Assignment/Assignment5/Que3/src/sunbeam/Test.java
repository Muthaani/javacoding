package sunbeam;

import java.util.Scanner;

import company.*;

public class Test {
	static Scanner sc=new Scanner(System.in);
	
	private static void acceptEmployee(Employee emp) {
		sc.nextLine();
		System.out.print("Enter employee first name    : ");
		emp.setfName(sc.nextLine());
		System.out.print("Enter employee last name     : ");
		emp.setlName(sc.nextLine());
		System.out.print("Enter Social Security Number : ");
		emp.setSSN(sc.nextInt());		
	}
	
	public static void acceptRecord(Employee emp) {
		acceptEmployee(emp);
		
		if(emp instanceof BasePlusCommissionEmployee ) {
			BasePlusCommissionEmployee bemp=(BasePlusCommissionEmployee)emp;
			 System.out.print("Enter base salary            : ");
			 bemp.setBaseSalary(sc.nextFloat());
			 System.out.print("Enter gross sales            : ");
			 bemp.setGrossSales(sc.nextFloat());
			 System.out.print("Enter commssion rates        : ");
			 bemp.setCommRate(sc.nextFloat());
		}
		else if(emp instanceof CommissionEmployee) {
			CommissionEmployee com=(CommissionEmployee)emp;
			System.out.print("Enter gross sales per week   : ");
			 com.setGrossSales(sc.nextFloat());
			 System.out.print("Enter commission rate        : ");
			 com.setCommRate(sc.nextFloat());
			
		}
		else if(emp instanceof HourlyEmployee) {
			HourlyEmployee hemp=(HourlyEmployee)emp;
			System.out.print("Enter hour wages             : ");
			hemp.setHourWages(sc.nextFloat());
			System.out.print("Enter work hour              : ");
			hemp.setHours(sc.nextFloat());	
		}
		
		else if(emp instanceof SalariedEmployee) {
			SalariedEmployee semp=(SalariedEmployee)emp;
			System.out.print("Enter weekly salary          : ");
			semp.setSalary(sc.nextFloat());	
		}
		
	}
	

	public static int menulist() {
		System.out.println("\n0. Exit\n1. Salaried Employee\n2. Hourly Employee\n3. Commission Employee\n4. Base Salaried Commission Employee");
		System.out.print("Enter choice : ");
		return sc.nextInt();
	}
	
	 public static  void printEmployee(Employee emp) {
			System.out.println("Employee first name          : "+emp.getfName());		
			System.out.println("Employee last name           : "+emp.getlName());		
			System.out.println("Social Security Number       : "+emp.getSSN());
				
		}
	 
	 public static void printRecord(Employee emp) {
		 
		 if(emp!=null) {
			 
			       printEmployee(emp);
			      
			     
			      if(emp instanceof BasePlusCommissionEmployee ) {
				 BasePlusCommissionEmployee bemp=(BasePlusCommissionEmployee)emp;
					 System.out.println("Total salary                 : "+(bemp.getBaseSalary()+bemp.getCommRate()*bemp.getGrossSales()/100));				
						
				     }
				else if(emp instanceof CommissionEmployee) {
					CommissionEmployee com=(CommissionEmployee)emp;
					System.out.println("Total Commission             : "+com.getCommRate()*com.getGrossSales()/100);		
					
					
				}
				else if(emp instanceof HourlyEmployee) {
					HourlyEmployee hemp=(HourlyEmployee)emp;
					float res=0.0f;			
					if(hemp.getHours()>40) {
						res=res+(hemp.getHours()-40)*1.5f*hemp.getHourWages();
						res=res+hemp.getHourWages()*40;
					}
					else
						res=hemp.getHours()*hemp.getHourWages();	
					System.out.println("Hourly salary                : "+res);		
					
				}
				
				else if(emp instanceof SalariedEmployee) {
					SalariedEmployee semp=(SalariedEmployee)emp;
					System.out.println("Fixed weekly salary          : "+semp.getSalary());
					
					
				}
			 		 
			 
		 }
		
	 }

	

}


