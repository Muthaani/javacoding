package sunbeam;

import company.*;


public class Program {
	
	private static Employee getinstance(int choice) {
		switch(choice) {
		case 1: return new SalariedEmployee();
		case 2: return new HourlyEmployee();
		case 3: return new CommissionEmployee();
		case 4: return new BasePlusCommissionEmployee();
		}
		return null;		
	}

	public static void main(String[] args) {
		 int choice=0;	 
		 while((choice=Test.menulist())!=0) {
			       Employee emp=getinstance(choice);			       			 
			       System.out.println();
				   Test.acceptRecord(emp);
				  System.out.println();				 
				  Test.printRecord(emp);
		 }		

		}
}
