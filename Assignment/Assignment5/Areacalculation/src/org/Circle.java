package org;

public class Circle extends Area{
	private float radius;
	
	public Circle() {}
	

	public float getRadius() {
		return radius;
		
	}
	public void setRadius(float radius) {
		this.radius = radius;
	}
	
	public final void calculateArea() {
		this.area=(float)(Math.PI*Math.pow(this.radius,2));
	}
	public final void calculatePerimeter() {
		this.perimeter=(float) Math.PI*2*this.radius;
	}
	
}