package org;

public class Rectangle extends Area{
	private float length;
	private float bredth;
	
	public Rectangle() {		
	}

	public float getLength() {
		return length;
	}

	public void setLength(float length) {
		this.length = length;
	}

	public float getBredth() {
		return bredth;
	}

	public void setBredth(float bredth) {
		this.bredth = bredth;
	}
	
	public final void calculateArea() {
		this.area=this.bredth*this.length;
	}
	
	public final void calculatePerimeter() {
		this.perimeter=2*(this.length+this.bredth);
	}

}
