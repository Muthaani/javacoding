package org;

public class Triangle extends Area {
 private float side1;
 private float side2;
 private float side3;
    
   public Triangle() {	
	   this.side1=0.0f;
	   this.side1=0.0f;
	   this.side3=0.0f;
    }

public float getSide1() {
	return side1;
}

public void setSide1(float side1) {
	this.side1 = side1;
}

public float getSide2() {
	return side2;
}

public void setSide2(float side2) {
	this.side2 = side2;
}

public float getSide3() {
	return side3;
}

public void setSide3(float side3) {
	this.side3 = side3;
}

public final void calculateArea() {
	float s=(this.side1+this.side2+this.side3)/2;
	this.area= (float)Math.sqrt(s*(s-this.side1)*(s-this.side2)*(s-this.side3));
}   

public final void calculatePerimeter() {
	this.perimeter=this.side1+this.side2+this.side3;
}
   
}
