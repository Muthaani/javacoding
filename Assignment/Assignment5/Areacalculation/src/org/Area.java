package org;

public abstract class Area {
	protected float area;
	protected float perimeter;
	
	public Area() {
	}
	
	public abstract void calculateArea();
	public abstract void calculatePerimeter();
	
	public final float getArea() {
		return this.area;
	}
	public final float getPerimeter() {
		return this.perimeter;
	}

}
