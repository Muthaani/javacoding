package org;

public class Address {
	private String cityName;
	private String streetName;
	private String pincode;
	
	public Address() {
		this.cityName="City Name";
		this.streetName="Street Name";
		this.pincode="Pincode";
	}
	public Address(String cityName,String streetName,String pincode) {
		this.cityName=cityName;
		this.streetName=streetName;
		this.pincode=pincode;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	
	

}
