package org;

public class Person {
	
	private String name;
	private Date date;
	private Address add;
	
	public Person(){
		this.name="Name";
		this.date=new Date();
		this.add=new Address();		
	}
	
	public Person(String name,Date date,Address add){
		this.name=name;
		this.date=date;
		this.add=add;		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Address getAdd() {
		return add;
	}

	public void setAdd(Address add) {
		this.add = add;
	}
	
	
	

}
