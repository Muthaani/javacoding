package person;

import java.util.Scanner;

import org.*;

public class Test {
	
	static Scanner sc=new Scanner(System.in);

	public static void main(String[] args) {
		Person p=new Person();
		accept(p);
		print(p);
	}

	private static void print(Person p) {
		System.out.println("\nFull name             : "+p.getName());
		System.out.print("Birth Date            : ");
		print(p.getDate());
		System.out.println("Address               ");
		print(p.getAdd());
		
		
	}

	private static void print(Address add) {	
	
		System.out.println("City name             : "+add.getCityName());
		System.out.println("Street name           : "+add.getStreetName());
		System.out.println("Pincode               : "+add.getPincode());		
	}

	private static void print(Date date) {
		System.out.println(date.getDay()+" / "+date.getMonth()+" / "+date.getYear());		
	}

	private static void accept(Person p) {
		System.out.print("Enter full name       : ");
		p.setName(sc.nextLine());
		System.out.println("Enter birth date      ");
		accept(p.getDate());
		System.out.println("Enter address          ");
		accept(p.getAdd());		
	} 
	
	public static void accept(Date d) {
		System.out.print("Enter day             : ");
		d.setDay(sc.nextInt());
		System.out.print("Enter month           : ");
		d.setMonth(sc.nextInt());
		System.out.print("Enter year            : ");
		d.setYear(sc.nextInt());
		
	}
	
    public static void accept(Address add) {
    	sc.nextLine();
    	System.out.print("Enter city name       : ");
    	add.setCityName(sc.nextLine());
		System.out.print("Enter street name     : ");
		add.setStreetName(sc.nextLine());
		System.out.print("Enter pincode         : ");
		add.setPincode(sc.nextLine());
	}

}
