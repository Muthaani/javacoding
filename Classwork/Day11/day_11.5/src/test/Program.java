package test;
class Base{
	private int num1;
	private int num2;
	public Base( ) {
		this.num1 = 0;
		this.num2 = 0;
	}
	public int getNum1() {
		return num1;
	}
	public void setNum1(int num1) {
		this.num1 = num1;
	}
	public int getNum2() {
		return num2;
	}
	public void setNum2(int num2) {
		this.num2 = num2;
	}
}
class Derived extends Base{
	private int num3;
	public Derived() {
		this.num3 = 0;
	}
	public int getNum3() {
		return num3;
	}
	public void setNum3(int num3) {
		this.num3 = num3;
	}
}
public class Program {
	public static void main(String[] args) {
		Base base = new Derived();	//Upcasting : OK
		base.setNum1(10);	//OK
		base.setNum2(20);	//OK
	
		Derived derived = (Derived) base;	//Downcasting
		derived.setNum3(30);	//OK
		
		System.out.println("Num1	:	"+base.getNum1());
		System.out.println("Num2	:	"+base.getNum2());
		System.out.println("Num3	:	"+derived.getNum3());
		
	}
}
