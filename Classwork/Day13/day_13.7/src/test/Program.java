package test;

//class Test is Resource Type
class Test implements AutoCloseable{
	public Test() {
	}
	@Override
	public void close() throws Exception {
	}
}
public class Program {
	public static void main(String[] args) {
		Test t = new Test( );
		//Test t;	=> reference
		//new Test( );	=> Instance : resource
	}
}
