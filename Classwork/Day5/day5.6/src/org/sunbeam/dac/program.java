package org.sunbeam.dac;
public class program {
	private int num1 = 10;
	private static int num2 = 20;
	public static void main(String[] args) {
		//System.out.println("Num1	:	"+num1);	//Not OK
		program p = new program();
		System.out.println("Num1	:	"+p.num1);	//OK : 10
		System.out.println("Num2	:	"+num2);	//OK : 20
	}
}