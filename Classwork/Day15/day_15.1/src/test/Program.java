package test;
interface A{	//ISI
	int number = 10;
	//public static final int number = 10;
	void print( ) ;
	//public abstract void print( ) ;
}
class B implements A{	//Service Provider
	@Override
	public void print() {
		System.out.println("Number	:	"+A.number);
	}
}
public class Program {	//Service Consumer
	public static void main(String[] args) {
		B b = new B( );
		b.print();	//OK : 10
		
		A a = new B( );	//Upcasting : Recommended
		a.print();	//OK : 10
	}
}
