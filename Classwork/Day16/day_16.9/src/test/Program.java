package test;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class ListTest{
	private List<Integer> list;
	public void setList(List<Integer> list) {
		this.list = list;
	}
	public void addRecord( int[] arr ) {
		if( arr != null ) {
			for (int element : arr)
				list.add(element);
		}
	}
	public Integer findRecord(int element) {
		Integer key = new Integer(element);
		if( list.contains(key)) {
			int index = list.indexOf(key);
			return index;
		}
		return null;
	}
	public boolean removedRecord(int element) {
		Integer key = new Integer(element);
		if( list.contains(key)) {
			list.remove(key);
			return true;
		}
		return false;
	}
	public void printRecord() {
		for (Integer element : list) {
			System.out.println(element);
		}
	}
}
public class Program {
	static Scanner sc = new Scanner(System.in);
	public static void acceptRecord( int[] key ) {
		System.out.print("Enter key	:	");
		key[ 0 ] = sc.nextInt();
	}
	public static void printRecord( Integer value ) {
		if( value != null )
			System.out.println("Key found at index : "+value);
		else
			System.out.println(" not found");
	}
	public static void printRecord( boolean removedStatus ) {
		if( removedStatus )
			System.out.println("Key is removed");
		else
			System.out.println("Key is not removed");
	}
	public static int menuList( ) {
		System.out.println("0.Exit");
		System.out.println("1.Insert Record");
		System.out.println("2.Find Record");
		System.out.println("3.Remove Record");
		System.out.println("4.Print Records");
		System.out.print("Enter choice	:	");
		return sc.nextInt();
	}
	public static void main(String[] args) {
		int choice;
		int[] key = new int[1];
		ListTest test = new ListTest();
		test.setList(new ArrayList<Integer>());
		while( ( choice = Program.menuList( ) ) != 0 ) {
			switch( choice ) {
			case 1:
				int[] arr = { 1,2,3,4,5,6,7,8,9,10};
				test.addRecord(arr);
				break;
			case 2:
				Program.acceptRecord(key);
				Integer value =  test.findRecord( key[ 0 ] );
				Program.printRecord(value);
				break;
			case 3:
				Program.acceptRecord(key);
				boolean removedStatus = test.removedRecord( key[ 0 ] );
				Program.printRecord(removedStatus);
				break;
			case 4:
				test.printRecord( );
				break;
			}
		}
	}
}
