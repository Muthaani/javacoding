package test;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Program {
	public static void main(String[] args) {
		System.out.println(new SimpleDateFormat("dd-MMMM,yyyy").format(new Date()));
	}
	public static void main3(String[] args) {
		Date date = new Date();
		String pattern = "dd-MMMM,yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		String strDate = sdf.format(date);
		System.out.println(strDate);
	}
	public static void main2(String[] args) {
		Date date = new Date();
		System.out.println(date.getDate()+" / "+ ( date.getMonth() + 1) +" / "+(date.getYear()+ 1900));	//Fri Dec 11 10:26:37 IST 2020
	}
	public static void main1(String[] args) {
		Date date = new Date();
		System.out.println(date.toString());	//Fri Dec 11 10:26:37 IST 2020
	}
}
