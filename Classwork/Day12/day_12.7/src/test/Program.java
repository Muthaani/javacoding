package test;

import java.util.Date;

public class Program {
	public static void main(String[] args) {
		Object obj = new Date();
		String str = (String) obj;	//Checking will be done at runtime. //ClassCastException
		
		String str1 = new Date( );	//Checking will be done at compile time.
	}
	public static void main4(String[] args) {
		Object obj = new Date( );	
		Date date = (Date) obj;		//Downcasting : It will work
		String str = (String) obj;	//Downcasting : ClassCastException
	}
	public static void main3(String[] args) {
		Object obj = null;
		String str = (String) obj;	//OK : Downcasting	
		Date date = (Date) obj;		//OK : Downcasting
		System.out.println(str + "	"+date);
	}
	public static void main2(String[] args) {
		Date dt1 = new Date(); //OK
		
		Object obj = new Date( ); //OK : Upcasting
		Date dt2 = (Date) obj;	//OK Downcasting
	}
	public static void main1(String[] args) {
		String str1 = new String();	//OK
		Object obj = new String();	//OK : Upcasting
		String str2 = (String) obj;	//OK : Downcasting 
	}
}
