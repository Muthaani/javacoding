package test;

import java.util.Scanner;

abstract class Shape{
	protected double area;
	public Shape() {
	}
	public abstract void calculateArea( );
	public final double getArea() {
		return this.area;
	}
}
class Rectangle extends Shape{
	private double length;
	private double breadth;
	public Rectangle() {
	}
	public void setLength(double length) {
		this.length = length;
	}
	public void setBreadth(double breadth) {
		this.breadth = breadth;
	}
	public void calculateArea( ) {
		this.area = this.length * this.breadth;
	}
}
class Circle extends Shape{ 
	private double radius;
	public Circle() {
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	public void calculateArea( ) {
		this.area = Math.PI * Math.pow(this.radius, 2);
	}
}
public class Program {
	static Scanner sc = new Scanner(System.in);
	private static void acceptRecord(Shape shape) {
		if( shape instanceof Rectangle ) {
			Rectangle rect = (Rectangle) shape;	//Downcasting
			System.out.print("Length	:	");
			rect.setLength(sc.nextDouble());
			System.out.print("Breadth	:	");
			rect.setBreadth(sc.nextDouble());
		}else {
			Circle c = (Circle) shape;
			System.out.print("Radius	:	");
			c.setRadius(sc.nextDouble());
		}
	}
	private static void printRecord(Shape shape) {
		System.out.println("Area	:	"+shape.getArea());
	}
	public static int menuList( ) {
		System.out.println("0.Exit");
		System.out.println("1.Rectangle");
		System.out.println("2.Circle");
		System.out.print("Enter choice	:	");
		return sc.nextInt();
	}
	public static void main(String[] args) {
		int choice;
		while( ( choice = Program.menuList( ) ) != 0 ) {
			Shape shape = null;
			switch( choice ) {
			case 1:
				shape = new Rectangle();	//Upcasting
				break;
			case 2:
				shape = new Circle();	//Upcasting
				break;
			}
			if( shape != null ) {
				Program.acceptRecord( shape );
				shape.calculateArea( );
				Program.printRecord( shape );
			}
		}
	}
}
