package test;
class Person{
	void print( int number ) {
		System.out.println("super.print()");
	}
}
class Employee extends Person{
	 void print( double number ) {
		System.out.println("sub.print()");
	}
}
public class Program {
	public static void main(String[] args) {
		Person p = new Employee();
		p.print( 10 );
	}
}
