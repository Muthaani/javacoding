package test;

import java.util.Date;

public class Program {
	public static void printRecord( Object obj ) {
		System.out.println(obj);
	}
	public static <T> void showRecord( T obj ) {
		System.out.println(obj);
	}
	public static <T extends Number > void print( T obj ) {
		System.out.println(obj);
	}
	public static void main(String[] args) {
		Program.print(true);
		Program.print('A');
		Program.print(123);
		Program.print(456.78);
		Program.print("Java");
		Program.print(new Date());
	}
}
