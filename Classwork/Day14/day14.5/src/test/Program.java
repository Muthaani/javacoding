package test;
class Test{
	private int number;
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
}
public class Program {
	public static void main(String[] args) {
		Test t = new Test( );
		t.setNumber("10");	//Not OK
		System.out.println("Number	:	"+t.getNumber());
	}
	public static void main1(String[] args) {
		Test t = new Test( );
		t.setNumber(10);
		System.out.println("Number	:	"+t.getNumber());
	}
}
