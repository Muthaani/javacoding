package test;

import java.util.ArrayList;
import java.util.Date;

public class Program {
	public static void print( ArrayList<? super Integer > list ) {
		for( Object element : list )
			System.out.println(element);
	}
	public static void main(String[] args) {
		ArrayList<Object> list = new ArrayList<>();
		list.add(10);
		list.add(10.5);
		
		Program.print(list);
	}
}
