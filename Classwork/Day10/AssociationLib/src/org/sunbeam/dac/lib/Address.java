package org.sunbeam.dac.lib;

public class Address {
	private String cityName;
	private String stateName;
	private String pincode;
	public Address() {
		this("","","");
	}
	public Address(String cityName, String stateName, String pincode) {
		this.cityName = new String(cityName);
		this.stateName = new String(stateName);
		this.pincode = new String(pincode);
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	
}
