package org.sunbeam.dac.lib;

public class Person {
	private String name;		//Association	
	private Date birthDate;		//Association
	private Address address;	//Association
	public Person() {
		this.name = new String( );
		this.birthDate  = new Date();
		this.address = new Address();
	}
	public Person(String name, int day, int month, int year, String cityName, String stateName, String pincode ) {
		this.name = new String( name );
		this.birthDate  = new Date( day, month, year );
		this.address = new Address( cityName, stateName, pincode );
	}
	public Person(String name, Date birthDate, Address address) {
		this.name = name;
		this.birthDate = birthDate;
		this.address = address;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
}

