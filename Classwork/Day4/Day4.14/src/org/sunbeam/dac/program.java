package org.sunbeam.dac;

class Complex{
	private int real;
	private int imag;
	public Complex( ) {
		this.real = 0;
		this.imag = 0;
	}
	public Complex(int real, int imag) {
		this.real = real;
		this.imag = imag;
	}
	public void printRecord() {
		System.out.println(this.real+"	"+this.imag);
	}
}
public class program {
	public static void main(String[] args) {
		Complex c1 = new Complex(10, 20);
		c1.printRecord();
		
		Complex c2 = new Complex( );
		c2.printRecord();
	}
}



