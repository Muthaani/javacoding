class Program{
    public static void main(String[] args){
        //printf("Hello World");
        
        //System : It is final class declared in java.lang Package
        //out : It is reference of instance(object) of PrintStream class. It is public static final field of System class.
        //print, printf, println : It is non static method of java.io.PrintStream class 

        System.out.println("Hello World");     
    }
}
